Name:             melonds-git
Summary:          DS emulator, sorta
Version:          REPLACEME_VERSION
Release:          1
License:          GPLv3+
URL:              http://melonds.kuribo64.net/
Source0:          https://gitlab.com/dirextric/melonds-git/-/archive/master/melonds-git-master.tar.gz

BuildRequires:    binutils
BuildRequires:    cmake
BuildRequires:    gcc-c++
BuildRequires:    pkgconfig(gtk+-3.0)
BuildRequires:    hicolor-icon-theme
BuildRequires:    pkgconfig(libarchive)
BuildRequires:    pkgconfig(libcurl)
BuildRequires:    pkgconfig(libpcap)
BuildRequires:    pkgconfig(slirp)
BuildRequires:    pkgconfig(sdl2)

%if 0%{?fedora}
BuildRequires:    qt5-qtbase-devel
BuildRequires:    qt5-qtdeclarative-devel
%endif

%if 0%{?suse_version}
BuildRequires:    libqt5-qtbase-devel
BuildRequires:    libqt5-qtdeclarative-devel
%endif

%description
melonDS is a Nintendo DS emulator.
Required firmware image files (bios7.bin, bios9.bin, firmware.bin) are not included.

%prep
gunzip -c ../SOURCES/melonds-git-master.tar.gz | tar -xo

%build
mkdir melonds-git-master/build
cd melonds-git-master/build
cmake \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    ..

%install
cd melonds-git-master/build
make -j$(nproc --all)
install -Dm 755 melonDS "%{buildroot}/%{_bindir}/melonDS"
install -Dm 644 ../res/net.kuribo64.melonDS.desktop "%{buildroot}/%{_datadir}/applications/net.kuribo64.melonDS.desktop"
install -Dm 644 ../res/icon/melon_256x256.png "%{buildroot}/%{_datadir}/icons/hicolor/256x256/apps/net.kuribo64.melonDS.png"

%files
%doc melonds-git-master/README.md
%license melonds-git-master/LICENSE
%{_bindir}/melonDS
%{_datadir}/applications/net.kuribo64.melonDS.desktop
%{_datadir}/icons/hicolor/256x256/apps/net.kuribo64.melonDS.png
